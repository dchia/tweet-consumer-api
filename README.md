# tweet-consumer-api

- Ejecución del proyecto:
*mvn spring-boot:run*

- Configuración para la ejecución:

    Es necesario configurar las siguientes variables de entorno:
    
    - twitter4j.oauth.accessToken
    - twitter4j.oauth.accessTokenSecret
    - twitter4j.oauth.user
    - twitter4j.oauth.password
    - twitter4j.oauth.consumerKey
    - twitter4j.oauth.consumerSecret
    
    O bien incluir un fichero twitter4j.properties que contenga esta información sin el prefijo twitter4j.
    
    Ref: http://twitter4j.org/en/configuration.html
    
    Los access token y los consumer keys se han de obtener con una cuenta de desarrollador de Twitter si se quieren 
    emplear contra un entorno real.
    
    De no poder o querer emplearse, se adjunta en el proyecto el fichero de una 
    base de datos H2 con información a la que poder acceder, para lo que habría que cambiar las variables:
    
    - spring.datasource.url
    - com.codechallenge.tweetconsumerapi.start-stream-on-startup
    
    Indicando en la primera la dirección de la BBDD, y en la segunda indicando su valor a false 
    para desactivar la lectura automática de tweets al arrancar el proyecto.
    
    Por ejemplo:
    
    *spring.datasource.url=jdbc:h2:~/tweet-consumer-api-db*
    
    
    *com.codechallenge.tweetconsumerapi.start-stream-on-startup=false*
