package com.codechallenge.tweetconsumerapi.models;

/**
 * Usa el estándar BCP 47, empleado por la API de Twitter, o 'und' si este no se ha podido detectar:
 * https://tools.ietf.org/html/bcp47 --> Documentación del estándar
 *
 * Listado completo incluído aquí:
 * https://support.gnip.com/apis/powertrack2.0/rules.html#Operators --> Documentación de la API de Twitter
 *
 */
public enum Language {
    AMHARIC("am"),
    ARABIC("am"),
    ARMENIAN("hy"),
    BENGALI("bn"),
    BULGARIAN("bg"),
    BURMESE("my"),
    CHINESE("zh"),
    CZECH("cs"),
    DANISH("da"),
    DUTCH("nl"),
    ENGLISH("en"),
    ESTONIAN("et"),
    FINISH("fi"),
    FRENCH("fr"),
    GEORGIAN("ka"),
    GERMAN("de"),
    GREEK("el"),
    GUJARATI("gu"),
    HAITIAN("ht"),
    HEBREW("iw"),
    HINDI("hi"),
    HUNGARIAN("hu"),
    ICELANDIC("is"),
    INDONESIAN("in"),
    ITALIAN("it"),
    JAPANESE("ja"),
    KANNADA("kn"),
    KHMER("km"),
    KOREAN("ko"),
    LAO("lo"),
    LATVIAN("lv"),
    LITHUANIAN("lt"),
    MALAYALAM("ml"),
    MALDIVIAN("dv"),
    MARATHI("mr"),
    NEPALI("ne"),
    NORWEGIAN("no"),
    ORIYA("or"),
    PANJABI("pa"),
    PASHTO("ps"),
    PERSIAN("fa"),
    POLISH("pl"),
    PORTUGUESE("pt"),
    ROMANIAN("ro"),
    RUSSIAN("ru"),
    SERBIAN("sr"),
    SINDHI("sd"),
    SINHALA("si"),
    SLOVAK("sk"),
    SLOVENIAN("sl"),
    SORANI_KURDISH("ckb"),
    SPANISH("es"),
    SWEDISH("sv"),
    TAGALOG("tl"),
    TAMIL("ta"),
    TELUGU("te"),
    THAI("th"),
    TIBETAN("bo"),
    TURKISH("tr"),
    UKRAINIAN("uk"),
    URDU("ur"),
    UYGHUR("ug"),
    VIETNAMESE("vi"),
    WELSH("cy"),

    UNDETECTED("und");

    private final String language;

    private Language(String languageValue) {
        language = languageValue;
    }

    @Override
    public String toString() {
        return language;
    }

    public static boolean isAValidLanguage(String languageToCheck) {
        if (languageToCheck != null && !languageToCheck.isEmpty() && (languageToCheck.length() == 2 || languageToCheck.length() == 3)) {
            for (Language languageInEnum : Language.values()) {
                if (languageInEnum.toString().equals(languageToCheck)) {
                    return true;
                }
            }
        }

        return false;
    }
}
