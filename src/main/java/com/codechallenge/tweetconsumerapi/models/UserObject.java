package com.codechallenge.tweetconsumerapi.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import twitter4j.User;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

/**
 * Creo una entidad independiente para los usuarios y realizo lo mismo que en TweetObject
 */
@Entity
@Data
@Table(name = "Users")
public class UserObject {

    @Id
    private Long userID;
    private String user;

    @OneToMany(mappedBy = "user")
    @JsonBackReference
    private List<TweetObject> tweets;

    private Integer numOfFollowers;

    public UserObject() {

    }

    public UserObject(User user) {
        this.userID = user.getId();
        this.user = user.getName();
        this.numOfFollowers = user.getFollowersCount();
    }
}
