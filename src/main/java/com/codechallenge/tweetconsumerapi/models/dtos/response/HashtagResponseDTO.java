package com.codechallenge.tweetconsumerapi.models.dtos.response;

import com.codechallenge.tweetconsumerapi.models.Hashtag;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Realizo lo mismo que en TweetObjectResponseDTO, en este caso para pasar del AtomicInteger a un entero.
 * En este caso no habría sido estrictamente necesario emplear esta clase, puesto que se tiene el converter,
 * pero se ha decidido hacerlo por consistencia.
 */
public class HashtagResponseDTO {

    @JsonProperty("hashtag")
    private String hashtagName;

    @JsonProperty("occurrences")
    private Integer occurrences;

    public HashtagResponseDTO() {

    }

    public HashtagResponseDTO(Hashtag hashtag) {
        this.hashtagName = hashtag.getHashtagName();
        this.occurrences = hashtag.getOccurrences().get();
    }
}
