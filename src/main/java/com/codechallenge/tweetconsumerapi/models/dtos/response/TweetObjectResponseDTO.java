package com.codechallenge.tweetconsumerapi.models.dtos.response;

import com.codechallenge.tweetconsumerapi.models.TweetObject;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Creo un DTO para devolver la información concreta que deseo y especificar cómo quiero mostrarla en el
 * JSON. Esto me permite mostrar sólo el ID del usuario y no toda la información relacionada, así como
 * agrupar las coordenadas, que se estaban guardando de forma individual.
 */
public class TweetObjectResponseDTO {

    @JsonProperty("tweet_id")
    private Long id;

    @JsonProperty("text")
    private String text;

    @JsonProperty("coordinates")
    private Coordinate coordinates;

    @JsonProperty("place")
    private String place;

    @JsonProperty("lang")
    private String lang;

    @JsonProperty("is_validated")
    private Boolean validated = false;

    @JsonProperty("user_id")
    private Long userID;


    public TweetObjectResponseDTO(TweetObject tweetObject) {
        if (tweetObject != null) {
            this.id = tweetObject.getId();
            this.text = tweetObject.getText();
            if (tweetObject.getCartesianCoordinateLatitude() != null && tweetObject.getCartesianCoordinateLongitude() != null) {
                coordinates = new Coordinate(tweetObject.getCartesianCoordinateLatitude(), tweetObject.getCartesianCoordinateLongitude());
            }

            this.place = tweetObject.getPlace();
            this.lang = tweetObject.getLang();
            this.validated = tweetObject.getValidated();
            this.userID = tweetObject.getUser().getUserID();
        }
    }
}
