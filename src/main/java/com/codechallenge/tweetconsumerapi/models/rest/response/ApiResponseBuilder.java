package com.codechallenge.tweetconsumerapi.models.rest.response;

import com.codechallenge.tweetconsumerapi.models.rest.response.metadata.ApiMetadata;
import com.codechallenge.tweetconsumerapi.models.rest.response.metadata.ApiPagingMetadata;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ApiResponseBuilder {

    public static <T> ApiResponse<T> with(HttpStatus status, boolean hasFinishedOK, String info) {
        ApiResponse<T> response = new ApiResponse<>();

        ApiResult result = new ApiResult();
        result.setStatus(hasFinishedOK);
        result.setInfo(info);
        result.setHttpCode(status.value());

        response.setResult(result);

        return response;
    }

    public static <T> ApiResponse<T> with(HttpStatus status, boolean hasFinishedOK, T data, String info) {
        ApiResponse<T> response = ApiResponseBuilder.with(status, hasFinishedOK, info);
        response.setData(data);
        return response;
    }

    public static <T> ApiResponse<T> withPaginationMetadata(HttpStatus status, boolean hasFinishedOK, T data, ApiPagingMetadata paging, String info) {
        ApiResponse<T> response = ApiResponseBuilder.with(status, hasFinishedOK, data, info);

        ApiMetadata metadata = new ApiMetadata();
        metadata.setPagination(paging);
        response.setMetadata(metadata);

        return response;
    }

    public static <T> ApiResponse<T> withError(HttpStatus status, ApiError error, String info) {
        ApiResponse<T> response = new ApiResponse<>();
        ApiResult result = new ApiResult();
        result.setStatus(false);
        result.setInfo(info);
        response.setResult(result);

        List<ApiError> errors = new ArrayList<>();
        errors.add(error);
        response.getResult().setErrors(errors);
        response.getResult().setHttpCode(status.value());

        return response;
    }

    public static <T> ApiResponse<T> withErrors(HttpStatus status, List<ApiError> errors, String info) {
        ApiResponse<T> response = new ApiResponse<>();
        ApiResult result = new ApiResult();
        result.setStatus(false);
        result.setInfo(info);
        result.setErrors(errors);
        result.setHttpCode(status.value());

        response.setResult(result);
        return response;
    }
}
