package com.codechallenge.tweetconsumerapi.models.rest.response;

import com.codechallenge.tweetconsumerapi.models.rest.response.metadata.ApiMetadata;
import lombok.Data;

@Data
public class ApiResponse<T> {

    private ApiResult result;

    private ApiMetadata metadata;

    private T data = null;

    public ApiResponse() {
        this.result = new ApiResult();
    }
}
