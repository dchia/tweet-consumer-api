package com.codechallenge.tweetconsumerapi.models.rest.response.metadata;

import lombok.Data;

@Data
public class ApiMetadata {

    private ApiPagingMetadata pagination;
}
