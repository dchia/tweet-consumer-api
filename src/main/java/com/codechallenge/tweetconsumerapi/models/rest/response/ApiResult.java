package com.codechallenge.tweetconsumerapi.models.rest.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

import java.util.List;

@JsonPropertyOrder({"status", "httpCode", "numElements", "info", "errors"})
@Data
public class ApiResult {
    @JsonProperty("status")
    private Boolean status;

    @JsonProperty("http_code")
    private Integer httpCode;

    @JsonProperty("num_elements")
    private Integer numElements;

    @JsonProperty("info")
    private String info;

    @JsonProperty("errors")
    private List<ApiError> errors;

    public ApiResult() {
        this.status = null;
        this.httpCode = null;
        this.info = null;
    }
}
