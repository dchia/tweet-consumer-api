package com.codechallenge.tweetconsumerapi.models.rest.response;

import lombok.Data;

@Data
public class ApiError {
    private String message;

    private ApiError() {

    }

    private ApiError(String message) {
        this.message = message;
    }

    public static ApiError withMessage(String message) {
        return new ApiError(message);
    }
}

