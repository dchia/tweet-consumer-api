package com.codechallenge.tweetconsumerapi.models.rest.response.metadata;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import static java.lang.Math.*;

/**
 * Clase empleada para mostrar la información de la paginación en el JSON de salida
 */
@Data
public class ApiPagingMetadata {

    @JsonProperty("actual_page")
    private Integer page;

    @JsonProperty("size_of_page")
    private Integer sizeOfPage;

    @JsonProperty("num_of_pages")
    private Long numOfPages;

    @JsonProperty("elements_in_page")
    private Long elementsInPage;

    @JsonProperty("total")
    private Long total;

    public ApiPagingMetadata(Integer page, Integer sizeOfPage, Long total) {
        this.page = page;
        this.sizeOfPage = sizeOfPage;
        this.total = total;
        this.numOfPages = floorDiv(total, sizeOfPage);

        if (Integer.toUnsignedLong(page) == numOfPages) {
            this.elementsInPage = total - (sizeOfPage * this.numOfPages);
        }
        else {
            this.elementsInPage = Integer.toUnsignedLong(this.sizeOfPage);
        }
    }
}
