package com.codechallenge.tweetconsumerapi.models.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Para evitar cualquier problema de concurrencia en columnas que sirvan de contador, creo este
 * Converter para poder usar variables del tipo AtomicInteger y realizar la conversión automática a Integer
 */
@Converter
public class AtomicIntConverter implements AttributeConverter<AtomicInteger, Integer> {

    @Override
    public Integer convertToDatabaseColumn(AtomicInteger attribute) {
        return attribute.get();
    }

    @Override
    public AtomicInteger convertToEntityAttribute(Integer dbData) {
        return new AtomicInteger(dbData);
    }
}