package com.codechallenge.tweetconsumerapi.models;

import lombok.Data;
import twitter4j.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Se ha optado por implementar una clase propia y leer los datos necesarios a partir del objeto de Tweet,
 * en lugar de usar algún tipo de adaptador, puesto que la información que se quiere es bastante concreta.
 *
 * Se ha optado por emplear el ID del tweet como identificador puesto que:
 * 1 - Se reduce la complejidad al evitar añadir un UUID propio del dominio de datos
 * 2 - Los ID de twitter parecen contener cierta información temporal (en qué orden se han creado) por lo que
 * esto permite ordenarlos o no por su orden de creación en las búsquedas
 */
@Entity
@Data
@Table(name = "Tweets")
public class TweetObject {

    @Id
    private Long id;

    @NotNull
    @Size(max = 500)
    private String text;

    private Double cartesianCoordinateLatitude;
    private Double cartesianCoordinateLongitude;
    private String place;

    private String lang;

    @ManyToOne
    @JoinColumn(name = "userID")
    private UserObject user;
    private Boolean validated = false;

    public TweetObject() {

    }

    public TweetObject(Status tweetToStore) {
        id = tweetToStore.getId();
        text = tweetToStore.getText();

        GeoLocation geoLocation = tweetToStore.getGeoLocation();
        if (geoLocation != null) {
            cartesianCoordinateLatitude = geoLocation.getLatitude();
            cartesianCoordinateLongitude = geoLocation.getLongitude();
        }

        if (tweetToStore.getPlace() != null) {
            place = tweetToStore.getPlace().getName();
        }

        lang = tweetToStore.getLang();

        User tweetUser = tweetToStore.getUser();
        UserObject userToStore = new UserObject();
        userToStore.setUserID(tweetUser.getId());
        userToStore.setUser(tweetUser.getName());
        userToStore.setNumOfFollowers(tweetUser.getFollowersCount());

        user = userToStore;
    }

    public void validate() {
        validated = true;
    }

    public void unValidate() {
        validated = false;
    }
}
