package com.codechallenge.tweetconsumerapi.models;

import com.codechallenge.tweetconsumerapi.models.converters.AtomicIntConverter;
import lombok.Data;

import javax.persistence.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * En este caso concreto se ha decidido emplear el ID del hashtag como identificador, puesto que se
 * pretende implementar una especie de Map, y añadir el UUID me ha parecido innecesario.
 */
@Data
@Entity
@Table(name = "Hashtags")
public class Hashtag {

    @Id
    @Column(name = "hashtag")
    private String hashtagName;

    @Convert(converter = AtomicIntConverter.class)
    private AtomicInteger occurrences;

    public Hashtag() {

    }

    public Hashtag(String hashtagText) {
        this.hashtagName = hashtagText;
        this.occurrences = new AtomicInteger(1);
    }

    public Hashtag(String hashtagName, int occurrences) {
        this.hashtagName = hashtagName;
        this.occurrences = new AtomicInteger(occurrences);
    }

    public void increaseOccurrencesBy(int occurrencesToSum) {
        if (occurrences != null) {
            occurrences.addAndGet(occurrencesToSum);
        }
    }
}
