package com.codechallenge.tweetconsumerapi.services.rest;

import com.codechallenge.tweetconsumerapi.exceptions.EmptyValueException;
import com.codechallenge.tweetconsumerapi.exceptions.TweetNotExists;
import com.codechallenge.tweetconsumerapi.models.dtos.response.HashtagResponseDTO;
import com.codechallenge.tweetconsumerapi.models.dtos.response.TweetObjectResponseDTO;
import com.codechallenge.tweetconsumerapi.models.rest.response.ApiResponse;
import com.codechallenge.tweetconsumerapi.models.rest.response.ApiResponseBuilder;
import com.codechallenge.tweetconsumerapi.models.rest.response.metadata.ApiPagingMetadata;
import com.codechallenge.tweetconsumerapi.services.TwitterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tweets")
public class TweetRestController {

    private TwitterService twitterService;

    private static final String VALIDATE_TWEET_OK_MESSAGE = "Successfully validated the tweet";
    private static final String MOST_USED_HASHTAGS_OK_MESSAGE = "Successfully retrieved the most used hashtags";
    private static final String VALIDATED_TWEETS_OF_USER_OK_MESSAGE = "Successfully retrieved the validated tweets of the user";
    private static final String RESTART_STREAMING_OK_MESSAGE = "Successfully authenticated and retrieving tweets";
    private static final String STOP_STREAMING_OK_MESSAGE = "Stopped the tweet stream. No more tweets are being recovered";

    @Autowired
    public TweetRestController(TwitterService twitterService) {
        this.twitterService = twitterService;
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseStatus(
            code = HttpStatus.PARTIAL_CONTENT
    )
    public ResponseEntity<ApiResponse<List<TweetObjectResponseDTO>>> retrieveStoredTweets(
            @RequestParam(value = "$page", required = false) Integer page,
            @RequestParam(value = "$limit", required = false) Integer limit
    ) throws EmptyValueException {
        List<TweetObjectResponseDTO> tweets = twitterService.retrieveTweets(page, limit);
        ApiPagingMetadata pagingMetadata = new ApiPagingMetadata(page, limit, twitterService.retrieveTotalNumberOfTweets());

        return new ResponseEntity<>(
                ApiResponseBuilder.withPaginationMetadata(
                        HttpStatus.PARTIAL_CONTENT,
                        true,
                        tweets,
                        pagingMetadata,
                        VALIDATE_TWEET_OK_MESSAGE
                ),
                HttpStatus.PARTIAL_CONTENT
        );
    }

    @PatchMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/validate/{tweet-id}"
    )
    @ResponseStatus(
            code = HttpStatus.OK
    )
    public ResponseEntity<ApiResponse<TweetObjectResponseDTO>> validateTweet(@PathVariable("tweet-id") Long id) throws EmptyValueException, TweetNotExists {
        TweetObjectResponseDTO validatedTweet = twitterService.validateTweet(id);

        return new ResponseEntity<>(
                ApiResponseBuilder.with(
                        HttpStatus.OK,
                        true,
                        validatedTweet,
                        VALIDATE_TWEET_OK_MESSAGE
                ),
                HttpStatus.OK
        );
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/hashtags"
    )
    @ResponseStatus(
            value = HttpStatus.OK
    )
    public ResponseEntity<ApiResponse<List<HashtagResponseDTO>>> getMostUsedHashtags(
            @RequestParam(value = "top", required = false) Integer numOfTopHashtagsToRetrieve
    ) {
        List<HashtagResponseDTO> mostUsedHashtags = twitterService.getMostUsedHashtags(numOfTopHashtagsToRetrieve);

        return new ResponseEntity<>(
                ApiResponseBuilder.with(
                        HttpStatus.OK,
                        true,
                        mostUsedHashtags,
                        MOST_USED_HASHTAGS_OK_MESSAGE
                ),
                HttpStatus.OK
        );
    }

    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/validated_by_user/{user-id}"
    )
    @ResponseStatus(
            value = HttpStatus.OK
    )
    public ResponseEntity<ApiResponse<List<TweetObjectResponseDTO>>> getUserValidatedTweets(@PathVariable("user-id") Long userId) {
        List<TweetObjectResponseDTO> validatedTweetsOfUser = twitterService.getValidatedTweetsOfUser(userId);

        return new ResponseEntity<>(
                ApiResponseBuilder.with(
                        HttpStatus.OK,
                        true,
                        validatedTweetsOfUser,
                        VALIDATED_TWEETS_OF_USER_OK_MESSAGE
                ),
                HttpStatus.OK
        );
    }

    /**
     * Método de utilidad que detiene el stream de tweets.
     * @return Respuesta OK
     */
    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/stop"
    )
    @ResponseStatus(
            value = HttpStatus.OK
    )
    public ResponseEntity<ApiResponse> stopStreaming() {
        twitterService.stopTweetStreaming();

        return new ResponseEntity<>(
                ApiResponseBuilder.with(
                        HttpStatus.OK,
                        true,
                        STOP_STREAMING_OK_MESSAGE
                ),
                HttpStatus.OK
        );
    }

    /**
     * Método de utilidad que reconecta con el API de Twitter.
     * Esto implica realizar de nuvo la autenticación.
     * @return Respuesta OK
     */
    @GetMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            value = "/start"
    )
    @ResponseStatus(
            value = HttpStatus.OK
    )
    public ResponseEntity<ApiResponse> restartStreaming() {
        twitterService.restartTweetStreaming();

        return new ResponseEntity<>(
                ApiResponseBuilder.with(
                        HttpStatus.OK,
                        true,
                        RESTART_STREAMING_OK_MESSAGE
                ),
                HttpStatus.OK
        );
    }
}
