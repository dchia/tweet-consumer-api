package com.codechallenge.tweetconsumerapi.services;

import com.codechallenge.tweetconsumerapi.config.TweetStorageConfiguration;
import com.codechallenge.tweetconsumerapi.exceptions.EmptyValueException;
import com.codechallenge.tweetconsumerapi.exceptions.TweetNotExists;
import com.codechallenge.tweetconsumerapi.models.Hashtag;
import com.codechallenge.tweetconsumerapi.models.TweetObject;
import com.codechallenge.tweetconsumerapi.models.dtos.response.HashtagResponseDTO;
import com.codechallenge.tweetconsumerapi.models.dtos.response.TweetObjectResponseDTO;
import com.codechallenge.tweetconsumerapi.repositories.HashtagsRepository;
import com.codechallenge.tweetconsumerapi.repositories.TweetObjectRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import twitter4j.*;
import java.util.*;

@Slf4j
@Service
public class TwitterService {

    private TwitterStream twitterStream;

    private TweetObjectRepository tweetObjectRepository;
    private HashtagsRepository hashtagsRepository;
    private TweetStorageConfiguration tweetStorageConfiguration;

    @Autowired
    public TwitterService(
            TweetObjectRepository tweetObjectRepository,
            HashtagsRepository hashtagsRepository,
            TweetStorageConfiguration tweetStorageConfiguration,
            TwitterStream twitterStream
    ) {
        this.tweetObjectRepository = tweetObjectRepository;
        this.hashtagsRepository = hashtagsRepository;
        this.tweetStorageConfiguration = tweetStorageConfiguration;
        this.twitterStream = twitterStream;
    }

    @Transactional(readOnly = true)
    public List<TweetObjectResponseDTO> retrieveTweets(Integer page, Integer limit) throws EmptyValueException {
        if (limit == null) {
            limit = tweetStorageConfiguration.getDefaultLimit();
        }
        if (page == null) {
            throw new EmptyValueException("Pagination page index is null");
        }

        Page<TweetObject> tweetsToRetrieve = tweetObjectRepository.findAll(PageRequest.of(page, limit));
        List<TweetObject> result = tweetsToRetrieve.getContent();
        List<TweetObjectResponseDTO> responseData = new ArrayList<>();

        for (TweetObject tweetObject : result) {
            responseData.add(new TweetObjectResponseDTO(tweetObject));
        }

        return responseData;
    }

    @Transactional(readOnly = true)
    public Long retrieveTotalNumberOfTweets() {
        return tweetObjectRepository.count();
    }

    @Transactional
    public TweetObjectResponseDTO validateTweet(Long tweetID) throws EmptyValueException, TweetNotExists {
        if (tweetID != null) {
            Optional<TweetObject> tweetObjectOptional = tweetObjectRepository.findById(tweetID);
            if (tweetObjectOptional.isPresent()) {
                TweetObject tweetObjectToValidate = tweetObjectOptional.get();
                tweetObjectToValidate.validate();
                return new TweetObjectResponseDTO(tweetObjectRepository.save(tweetObjectToValidate));
            }
            else {
                throw new TweetNotExists(tweetID);
            }
        }
        else {
            throw new EmptyValueException("Tweet identifier to validate");
        }
    }

    @Transactional(readOnly = true)
    public List<HashtagResponseDTO> getMostUsedHashtags(Integer numOfTopHashtagsToRetrieve) {
        if (numOfTopHashtagsToRetrieve == null) {
            numOfTopHashtagsToRetrieve = tweetStorageConfiguration.getNumberOfHastagsMostUsed();
        }

        List<Hashtag> hashtagsRetrieved = hashtagsRepository
                .getNMostUsedHashtagsOrderedByOccurrences(
                        PageRequest.of(
                                0,
                                numOfTopHashtagsToRetrieve
                        )
                );

        List<HashtagResponseDTO> result = new ArrayList<>(hashtagsRetrieved.size());
        for (Hashtag hashtag : hashtagsRetrieved) {
            result.add(new HashtagResponseDTO(hashtag));
        }

        return result;
    }

    @Transactional(readOnly = true)
    public List<TweetObjectResponseDTO> getValidatedTweetsOfUser(Long userID) {
        List<TweetObject> validatedTweets = tweetObjectRepository.getValidatedTweetsOfUser(userID);
        List<TweetObjectResponseDTO> response = new ArrayList<>();
        for (TweetObject tweetObject : validatedTweets) {
            response.add(new TweetObjectResponseDTO(tweetObject));
        }

        return response;
    }

    public void stopTweetStreaming() {
        this.twitterStream.shutdown();
    }

    public void restartTweetStreaming() {
        this.twitterStream.sample();
    }
}

