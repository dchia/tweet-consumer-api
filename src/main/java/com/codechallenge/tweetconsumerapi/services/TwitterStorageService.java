package com.codechallenge.tweetconsumerapi.services;

import com.codechallenge.tweetconsumerapi.config.TweetStorageConfiguration;
import com.codechallenge.tweetconsumerapi.models.Hashtag;
import com.codechallenge.tweetconsumerapi.models.TweetObject;
import com.codechallenge.tweetconsumerapi.models.UserObject;
import com.codechallenge.tweetconsumerapi.repositories.HashtagsRepository;
import com.codechallenge.tweetconsumerapi.repositories.TweetObjectRepository;
import com.codechallenge.tweetconsumerapi.repositories.UserObjectRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import twitter4j.HashtagEntity;
import twitter4j.Status;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class TwitterStorageService {

    private TweetStorageConfiguration tweetStorageConfiguration;
    private UserObjectRepository userObjectRepository;
    private TweetObjectRepository tweetObjectRepository;
    private HashtagsRepository hashtagsRepository;

    public TwitterStorageService(
            TweetStorageConfiguration tweetStorageConfiguration,
            UserObjectRepository userObjectRepository,
            TweetObjectRepository tweetObjectRepository,
            HashtagsRepository hashtagsRepository
    ) {
        this.tweetStorageConfiguration = tweetStorageConfiguration;
        this.userObjectRepository = userObjectRepository;
        this.tweetObjectRepository = tweetObjectRepository;
        this.hashtagsRepository = hashtagsRepository;
    }

    @Transactional
    public void storeNewTweet(Status status) {
        if (status != null &&
                tweetStorageConfiguration.isAnAllowedLanguage(status.getLang()) &&
                status.getUser() != null &&
                status.getUser().getFollowersCount() >= tweetStorageConfiguration.getMinimumNumberOfFollowers()
        ) {
            TweetObject tweetDataToStore = new TweetObject(status);
            if (!userObjectRepository.findById(status.getUser().getId()).isPresent()) {
                UserObject userToStore = new UserObject(status.getUser());
                userObjectRepository.saveAndFlush(userToStore);
            }

            tweetObjectRepository.saveAndFlush(tweetDataToStore);

            Set<String> hashtagTextSet = retrieveSetOfHashtagsFromTweet(status);

            if (!hashtagTextSet.isEmpty()) {
                saveNewHashtagsAndUpdateRepeatedOnes(hashtagTextSet);
            }
        }
    }

    @Transactional
    public void saveNewHashtagsAndUpdateRepeatedOnes(Set<String> hashtagTextSet) {
        Iterator<Hashtag> hashtags = hashtagsRepository.findAllById(hashtagTextSet).iterator();
        List<Hashtag> hashtagsToStore = new ArrayList<>();

        while (hashtags.hasNext()) {
            Hashtag hashtagToUpdate = hashtags.next();
            hashtagToUpdate.increaseOccurrencesBy(1);
            hashtagsToStore.add(hashtagToUpdate);
            hashtagTextSet.remove(hashtagToUpdate.getHashtagName());
        }

        if (!hashtagTextSet.isEmpty()) {
            for (String hashtagText : hashtagTextSet) {
                Hashtag newHashtag = new Hashtag();
                newHashtag.setHashtagName(hashtagText);
                newHashtag.setOccurrences(new AtomicInteger(1));
                hashtagsToStore.add(newHashtag);
            }
        }

        hashtagsRepository.saveAll(hashtagsToStore);
    }

    private Set<String> retrieveSetOfHashtagsFromTweet(Status status) {
        HashtagEntity[] hashtagEntities = status.getHashtagEntities();
        Set<String> hashtagsTextSet = new HashSet<>();

        for (HashtagEntity hashtagEntity : hashtagEntities) {
            if (hashtagEntity != null) {
                String hashtagText = String.format("#%s", hashtagEntity.getText());
                hashtagsTextSet.add(hashtagText);
            }
        }

        return hashtagsTextSet;
    }
}
