package com.codechallenge.tweetconsumerapi.services;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.*;

@Component
@Slf4j
public class TwitterObjectListener implements StatusListener {

    private TwitterStorageService storageService;

    @Autowired
    public TwitterObjectListener(TwitterStorageService storageService) {
        this.storageService = storageService;
    }

    @SneakyThrows
    @Override
    public void onStatus(Status status) {
        storageService.storeNewTweet(status);
        log.debug("Stored succesfully tweet: {}", status.toString());

    }

    @Override
    public void onDeletionNotice(StatusDeletionNotice statusDeletionNotice) {
        log.debug(statusDeletionNotice.toString());
    }

    @Override
    public void onTrackLimitationNotice(int i) {
        log.debug("onTrackLimitationNotice(): " + i);
    }

    @Override
    public void onScrubGeo(long l, long l1) {
        log.debug("onScrubGeo(): " + l + ", " + l1);
    }

    @Override
    public void onStallWarning(StallWarning stallWarning) {
        log.debug("onStallWarning(): " + stallWarning.toString());
    }

    @SneakyThrows
    @Override
    public void onException(Exception e) {
        throw e;
    }
}
