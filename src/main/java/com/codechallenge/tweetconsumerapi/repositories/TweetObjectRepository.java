package com.codechallenge.tweetconsumerapi.repositories;

import com.codechallenge.tweetconsumerapi.models.TweetObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TweetObjectRepository extends JpaRepository<TweetObject, Long> {

    @Query("select tweet from TweetObject tweet where tweet.validated = true and tweet.user.id = :id")
    List<TweetObject> getValidatedTweetsOfUser(@Param("id") Long userID);

    /**
     * Se sobreescribe el método de paginación por defecto para incluir la ordenación de los tweets por ID
     * @param pageable
     * @return Tweets page
     */
    @Override
    @Query("select tweet from TweetObject tweet order by tweet.id asc")
    Page<TweetObject> findAll(Pageable pageable);
}
