package com.codechallenge.tweetconsumerapi.repositories;

import com.codechallenge.tweetconsumerapi.models.Hashtag;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HashtagsRepository extends CrudRepository<Hashtag, String> {

    @Query("select hashtag from Hashtag hashtag order by hashtag.occurrences desc")
    List<Hashtag> getNMostUsedHashtagsOrderedByOccurrences(Pageable numOfHashtags);
}
