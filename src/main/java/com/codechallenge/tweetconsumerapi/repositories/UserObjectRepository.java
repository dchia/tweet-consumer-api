package com.codechallenge.tweetconsumerapi.repositories;

import com.codechallenge.tweetconsumerapi.models.UserObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserObjectRepository extends JpaRepository<UserObject, Long> {
}
