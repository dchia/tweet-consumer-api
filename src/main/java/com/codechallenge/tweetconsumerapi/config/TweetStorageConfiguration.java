package com.codechallenge.tweetconsumerapi.config;

import com.codechallenge.tweetconsumerapi.models.Language;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Configuration
@Data
@Slf4j
public class TweetStorageConfiguration {

    @Value("${com.codechallenge.tweetconsumerapi.minimum-number-of-followers:1500}")
    private Integer minimumNumberOfFollowers;

    @Value("${com.codechallenge.tweetconsumerapi.number-of-hashtags-most-used:10}")
    private Integer numberOfHastagsMostUsed;

    @Value("${com.codechallenge.tweetconsumerapi.allowed-langs:es,fr,it}")
    private List<String> allowedLanguages;

    @Value("${com.codechallenge.tweetconsumerapi.limit-by-default:25}")
    private Integer defaultLimit;

    @Value("${com.codechallenge.tweetconsumerapi.start-stream-on-startup:true}")
    private Boolean initStreamImmediately;

    @PostConstruct
    private void checkLanguages() {
        if (allowedLanguages != null && !allowedLanguages.isEmpty()) {
            List<String> checkedLanguages = new ArrayList<>();
            for (String userInputLanguage : allowedLanguages) {
                if (Language.isAValidLanguage(userInputLanguage)) {
                    checkedLanguages.add(userInputLanguage);
                }
                else {
                    log.error("The language {} is not a valid language. Please, refer to BCP 47 tags", userInputLanguage);
                }
            }

            if (checkedLanguages.isEmpty()) {
                checkedLanguages = getDefaultLanguages();
            }

            allowedLanguages = checkedLanguages;
        }
        else {
            allowedLanguages = getDefaultLanguages();
        }
    }

    private List<String> getDefaultLanguages() {
        List<String> defaultLanguages = new ArrayList<>();
        defaultLanguages.add(Language.SPANISH.toString());
        defaultLanguages.add(Language.FRENCH.toString());
        defaultLanguages.add(Language.ITALIAN.toString());
        return defaultLanguages;
    }

    public boolean isAnAllowedLanguage(String languageToCheck) {
        return allowedLanguages.contains(languageToCheck);
    }
}
