package com.codechallenge.tweetconsumerapi.config;

import com.codechallenge.tweetconsumerapi.services.TwitterObjectListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;

@Configuration
public class TwitterStreamInitializer {

    private TwitterStream twitterStream;
    private TwitterObjectListener twitterObjectListener;
    private TweetStorageConfiguration tweetStorageConfiguration;

    @Autowired
    public TwitterStreamInitializer(
            TwitterObjectListener twitterObjectListener,
            TweetStorageConfiguration storageConfiguration
    ) {
        this.twitterObjectListener = twitterObjectListener;
        this.tweetStorageConfiguration = storageConfiguration;
    }

    @Bean
    @Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
    public TwitterStream initTwitterStreamAndKeepListening() {
        twitterStream = new TwitterStreamFactory().getInstance().addListener(twitterObjectListener);

        // sample() method internally creates a thread which manipulates TwitterStream and calls these adequate listener methods continuously
        if (tweetStorageConfiguration.getInitStreamImmediately()) {
            twitterStream.sample();
        }

        return twitterStream;
    }
}
