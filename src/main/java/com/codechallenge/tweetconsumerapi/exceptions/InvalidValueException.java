package com.codechallenge.tweetconsumerapi.exceptions;

public class InvalidValueException extends Exception {

    private static final String ERROR_MESSAGE = "The value was incorrect. Error: %s";

    public InvalidValueException(String affectedValue) {
        super(String.format(ERROR_MESSAGE, affectedValue));
    }
}
