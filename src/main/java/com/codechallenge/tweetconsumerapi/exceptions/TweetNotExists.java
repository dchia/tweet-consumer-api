package com.codechallenge.tweetconsumerapi.exceptions;

public class TweetNotExists extends Exception {

    private static final String ERROR_MESSAGE = "There is no tweet with the identifier: %d";

    public TweetNotExists(Long tweetID) {
        super(String.format(ERROR_MESSAGE, tweetID));
    }
}
