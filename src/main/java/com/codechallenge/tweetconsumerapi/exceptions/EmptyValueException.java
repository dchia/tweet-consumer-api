package com.codechallenge.tweetconsumerapi.exceptions;

public class EmptyValueException extends Exception {

    private static final String ERROR_MESSAGE = "The value was null or empty. Error: %s";

    public EmptyValueException(String affectedValue) {
        super(String.format(ERROR_MESSAGE, affectedValue));
    }
}
