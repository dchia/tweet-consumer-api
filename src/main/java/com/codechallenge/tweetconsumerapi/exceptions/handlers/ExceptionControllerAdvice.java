package com.codechallenge.tweetconsumerapi.exceptions.handlers;

import com.codechallenge.tweetconsumerapi.exceptions.EmptyValueException;
import com.codechallenge.tweetconsumerapi.exceptions.InvalidValueException;
import com.codechallenge.tweetconsumerapi.exceptions.TweetNotExists;
import com.codechallenge.tweetconsumerapi.models.rest.response.ApiError;
import com.codechallenge.tweetconsumerapi.models.rest.response.ApiResponse;
import com.codechallenge.tweetconsumerapi.models.rest.response.ApiResponseBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ExceptionControllerAdvice {
    private static final String EMPTY_VALUE_EXCEPTION_ERROR_MESSAGE = "An empty or null value has been found";
    private static final String INVALID_VALUE_EXCEPTION_ERROR_MESSAGE = "An invalid value has been found";
    private static final String TWEET_NOT_EXISTS_EXCEPTION_ERROR_MESSAGE = "The requested tweet does not exist";
    private static final String EXCEPTION_ERROR_MESSAGE = "Unrecognized exception found. Please, contact the software provider";


    @ExceptionHandler(EmptyValueException.class)
    public ResponseEntity<ApiResponse> buildEmptyValueApiResponse(EmptyValueException e) {
        return buildErrorApiResponse(e, HttpStatus.BAD_REQUEST, EMPTY_VALUE_EXCEPTION_ERROR_MESSAGE);
    }

    @ExceptionHandler(InvalidValueException.class)
    public ResponseEntity<ApiResponse> buildInvalidValueApiResponse(InvalidValueException e) {
        return buildErrorApiResponse(e, HttpStatus.BAD_REQUEST, INVALID_VALUE_EXCEPTION_ERROR_MESSAGE);
    }

    @ExceptionHandler(TweetNotExists.class)
    public ResponseEntity<ApiResponse> buildTweetNotExistsExceptionApiResponse(TweetNotExists e) {
        return buildErrorApiResponse(e, HttpStatus.NOT_FOUND, TWEET_NOT_EXISTS_EXCEPTION_ERROR_MESSAGE);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> buildNonManagedExceptionApiResponse(Exception e) {
        return buildErrorApiResponse(e, HttpStatus.INTERNAL_SERVER_ERROR, EXCEPTION_ERROR_MESSAGE);
    }

    private ResponseEntity<ApiResponse> buildErrorApiResponse(Exception e, HttpStatus status, String errorMessage) {
        ApiError apiError = ApiError.withMessage(e.getMessage());
        return new ResponseEntity<>(
                ApiResponseBuilder
                        .withError(
                                status,
                                apiError,
                                errorMessage
                        ),
                status
        );
    }
}
