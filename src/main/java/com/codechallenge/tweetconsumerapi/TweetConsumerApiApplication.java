package com.codechallenge.tweetconsumerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TweetConsumerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TweetConsumerApiApplication.class, args);
	}

}
