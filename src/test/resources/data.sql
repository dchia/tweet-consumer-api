-- Este script es cargado por el contexto de los test de Spring, y ejecutado a la vez que se arranca el contexto.
-- Esto permite inicializar la base de datos con datos cargados por defecto.
-- Otra opción hubiese sido añadir Flyway y mantener un versionado.
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test1', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test2', 24);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test3', 8);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test4', 6);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test5', 40);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test6', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test7', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test8', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test9', 3);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test10', 2);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test11', 5);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test12', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test13', 1);
INSERT INTO Hashtags (hashtag, occurrences) VALUES ('#test14', 4);