package com.codechallenge.tweetconsumerapi;

import com.codechallenge.tweetconsumerapi.config.TweetStorageConfiguration;
import com.codechallenge.tweetconsumerapi.models.Hashtag;
import com.codechallenge.tweetconsumerapi.repositories.HashtagsRepository;
import com.codechallenge.tweetconsumerapi.services.TwitterService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class TweetConsumerApiApplicationTests {

	@Autowired
	private HashtagsRepository hashtagsRepository;

	@Autowired
	private TweetStorageConfiguration storageConfiguration;

	@Autowired
	private TwitterService service;

	private final Integer numOfHashtagsToRetrieve = 8;

	@Test
	public void contextLoads() {
	}

	@Test
	public void dataIsLoaded() {
		assertEquals(15, hashtagsRepository.count());
	}

	@Test
	public void propertiesAreLoaded() {
		assertEquals(8, storageConfiguration.getNumberOfHastagsMostUsed());
	}

	@Test
	public void given_loadedHashtagsInDatabase_when_retrieveTheMostUsed_then_OK() {
		List<Hashtag> hashtags = hashtagsRepository.getNMostUsedHashtagsOrderedByOccurrences(
				PageRequest.of(0, storageConfiguration.getNumberOfHastagsMostUsed()));
		List<Hashtag> expectedHashtags = getExpectedHashtags();

		assertNotNull(hashtags);
		assertEquals(numOfHashtagsToRetrieve, hashtags.size());

		for (int i = 0; i < hashtags.size(); i++) {
			assertEquals(expectedHashtags.get(i).getHashtagName(), hashtags.get(i).getHashtagName());
			assertEquals(expectedHashtags.get(i).getOccurrences().get(), hashtags.get(i).getOccurrences().get());
		}
	}

	private List<Hashtag> getExpectedHashtags() {
		List<Hashtag> expectedResult = new ArrayList<>();
		Hashtag test1 = new Hashtag("#test5", 40);
		expectedResult.add(test1);

		Hashtag test2 = new Hashtag("#test2", 24);
		expectedResult.add(test2);

		Hashtag test3 = new Hashtag("#test3", 8);
		expectedResult.add(test3);

		Hashtag test4 = new Hashtag("#test4", 6);
		expectedResult.add(test4);

		Hashtag test5 = new Hashtag("#test11", 5);
		expectedResult.add(test5);

		Hashtag test6 = new Hashtag("#test14", 4);
		expectedResult.add(test6);

		Hashtag test7 = new Hashtag("#test9", 3);
		expectedResult.add(test7);

		Hashtag test8 = new Hashtag("#test10", 2);
		expectedResult.add(test8);
		return expectedResult;
	}
}
